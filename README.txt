#############################################
#####  evaluatingAssemblies README.txt  #####
#############################################

Repository:
https://bitbucket.org/gallaher/evaluatingassemblies

Author: 
Sean Gallaher

Version:
v1.3

Date:
29-NOV-2017


This repository contains three software tools
that were designed to help evaluate the relative 
quality of different assembly versions based on 
the alignment of high-throughput sequencing data. 

It was designed to aid in a project to compare
four different assembly versions of the chloroplast 
genome of the algal species, 
Chlamydomonas reinhardtii, but the tools should
be broadly applicable to other projects for 
which you have paired-end, Illumina-style 
sequencing data. 

You can read more about this approach and its
application in our paper in The Plant Journal:

http://onlinelibrary.wiley.com/doi/10.1111/tpj.13788/abstract

The bin directory consists of three software tools. 
They are written in PERL, and should run on as-is 
in a standard *nix-style terminal. 

The scripts are:

1. sam2errorFreq.pl
     This is a script that takes a sorted SAM file
     and the corresponding FASTA file as input, 
     and uses them to calculate the error frequency
     in 1000 nt windows. The results are output 
     as a bedgraph file.

2. bam2covDep.pl
     This is a script to generate a table
     of the percentage of loci whose depth
     of coverage fall within various bins relative
     to the mean coverage over 1000 nt windows
     from a sorted sam file of DNA-Seq reads. This
     tool expects that bedtools is installed and
     is available in your ${PATH}.
     See:
          https://github.com/arq5x/bedtools2
     to download and install bedtools.

3. sam2length.pl
     This is a script to generate a data file
     of the percentage of reads that fall
     within various bins relative to the mean
     for windows of size 1000 nt. It takes as 
     input a sorted sam file of paired-end reads.


Please send comments, questions, or trouble-shooting
requests to the author, Sean Gallaher, at

gallaher@chem.ucla.edu

This software is made available for free under the 
GNU GENERAL PUBLIC LICENSE. A copy of the license 
should be included with the repository.
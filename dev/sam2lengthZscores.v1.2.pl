#!/usr/bin/perl

use strict;
use warnings;

# This is a script to generate a bedgraph track
# of insert lengths Z-scores from a sam file of 
# pair-end reads.
# author: Sean D. Gallaher
# date:  31-MAR-2017
# filename: sam2lengthZscores.pl
# version: v1.2

# average accross windows of this many NTs
my $win = 100;

# Output this help in case of error 
my $help = "\nPlease supply a sorted sam file of\npaired-end DNA-Seq data as an arguement.\n\n";

# Get the name of a sam file, or else exit with help.
my $sam = shift @ARGV;
unless (defined $sam) {
	die $help;
}

# Create a name for the output file.
$sam =~ m/(.+).sam/;
my $root = $1;
my $outfile = $root . ".fragment_length_z-scores.bedgraph";


unless (defined $root) {
	die $help;
}

# Open the sam file and determine the median and standard deviation
# for the insert sizes for each position and put it into %master

open (IN, "<", $sam)
	or die "Cannot open $sam: $!\n";

my %master;
my @curPos;
my $lastPos = 0;
my $chrom;
my $curChrom;
my $length;
my $curLength;


my $maxInsert = 5000;

while (my $line = <IN>) {
	chomp $line;
	if ($line !~ m/^@/) {
		my @lineArray = split (/\t/, $line);
		$chrom = $lineArray[2];
		if ($chrom eq "*") {
			next;
		}
		if (!defined $curChrom) {
			$curChrom = $chrom;
		}
		my $position = $lineArray[3];
		$length = abs $lineArray[8];
		if ($lastPos == 0) {
			$lastPos = $position;
		}
		if ($chrom eq $curChrom) {
			if ($length > 0) {
				if ($position > $lastPos) {
					my $rawPosMean = &average (\@curPos);
					my $posMean = sprintf ("%.0f", $rawPosMean);
					@{$master{$chrom}}[$lastPos] = $posMean;
					@curPos = ();
					$lastPos = $position;
					if ($length < $maxInsert) {
						push (@curPos, $length);
					}
				}
				else {
					if ($length < $maxInsert) {
						push (@curPos, $length);
					}
				}
			}
		}
		else {
			my $rawPosMean = &average (\@curPos);
			my $posMean = sprintf ("%.0f", $rawPosMean);
			@{$master{$curChrom}}[$lastPos] = $posMean;
			$lastPos = $position;
			@curPos = ();
			$curChrom = $chrom;
			if ($length < $maxInsert) {
				push (@curPos, $length);
			}
		}
	}
}

if ($chrom ne "*") {
	if ($length < $maxInsert) {
		push (@curPos, $length);
	}

	my $rawPosMean = &average (\@curPos);
	my $posMean = sprintf ("%.0f", $rawPosMean);
	@{$master{$chrom}}[$lastPos] = $posMean;
}

close (IN);

# create the output file

open (OUT, ">", $outfile) 
	or die "Cannot open $outfile: $!\n\n";


print OUT "track type=bedGraph\n";

# for each position determine the Z-score and output to OUT

foreach my $chrom (sort keys %master) {
	my $null = shift @{$master{$chrom}};
	my $chromMean = &average($master{$chrom});
	my $chromStdev = &stdev($master{$chrom});
	my $chromLength = scalar @{$master{$chrom}};
	my $combinedLength;
	print STDOUT "chromosome = $chrom\nmean = $chromMean\nstdev = $chromStdev\n\n";
	for (my $n = 0; $n < $chromLength; $n++) {
		my $m = $n + 1;
		my $winDenominator = 0;
		if ($m%$win == 0) {		
			my $meanLength = $combinedLength / $win;
			my $rawZscore = ( $meanLength - $chromMean ) / $chromStdev ;
			my $zScore = sprintf ("%.3f", $rawZscore);
			my $start = $m - $win + 1;
			my $end = $m;
			print OUT "$chrom\t$start\t$end\t$zScore\n";
			if (defined @{$master{$chrom}}[$n]) {
				$combinedLength = @{$master{$chrom}}[$n];
				$winDenominator++;
			}
		}
		else {
			if (defined @{$master{$chrom}}[$n]) {
				$combinedLength += @{$master{$chrom}}[$n];
				$winDenominator++;
			}
		}
	}
}

close (OUT);


##### Subroutines #####

# adapted from https://edwards.sdsu.edu/research/calculating-the-average-and-standard-deviation/

sub average{
        my $data = shift @_;
        my $total = 0;
	my $count = 0;
	my $average;
        foreach my $value (@{$data}) {
		if (defined $value) {
	                $total += $value;
			$count++;
		}
        }
	if ($count > 0) {
	        $average = $total / $count;
	}
	else {
		$average = 0;
	}
        return $average;
}
sub stdev{
        my $data = shift @_;
        my $average = &average($data);
        my $sqtotal = 0;
	my $count = 0;
        foreach my $value (@{$data}) {
		if (defined $value) {
                	$sqtotal += ($average-$value) ** 2;
			$count++;
		}
        }
	if ($count < 2) {
		return 0;
	}
        else {
		my $std = ($sqtotal / ($count - 1)) ** 0.5;
        	return $std;
	}
}

#!/usr/bin/perl

use strict;
use warnings;

# This is a script to generate a bedgraph track
# of coverage depth Z-scores from a sam file of 
# DNA-Seq reads.
# author: Sean D. Gallaher
# date:  31-MAR-2017
# filename: bam2covdepZscores.pl
# version: v1.1

# average accross windows of this many NTs
my $win = 100;

# Output this help in case of error 
my $help = "\nPlease supply a sorted bam file of\nDNA-Seq data as an arguement.\n\nScript expects bedtools is in your \$PATH\n\n";

# Get the name of a bam file, or else exit with help.
my $bam = shift @ARGV;
unless (defined $bam) {
	die $help;
}

# Create a name for the output file.
$bam =~ m/(.+).bam/;
my $root = $1;
my $baseReport = $root . ".per-base_report.txt";
my $outfile = $root . ".coverage_depth_z-scores.bedgraph";


unless (defined $root) {
	die $help;
}

# If necessary, use bedtools to make a per-base repport of coverage depth.

if (!-e $baseReport) {
	my $bashCommand = qq {bedtools genomecov -d -split -ibam $bam > $baseReport };  
	system $bashCommand ;
}

# Open the baseReport, and transfer the
# the coverage per position to %master

my %master;

open (REPORT, "<", $baseReport) 
	or die "Cannot open $baseReport: $!\n\n$help";

while (my $line = <REPORT>) {
	chomp $line;
	my @lineArray = split (/\t/, $line);
	my $chrom = $lineArray[0];
	my $pos = $lineArray[1];
	my $cov = $lineArray[2];
	@{$master{$chrom}}[$pos]=$cov;
}

close (REPORT);

# create the output file

open (OUT, ">", $outfile) 
	or die "Cannot open $outfile: $!\n\n";


print OUT "track type=bedGraph\n";

# for each position determine the Z-score and output to OUT

foreach my $chrom (sort keys %master) {
	my $null = shift @{$master{$chrom}};
	my $chromMean = &average($master{$chrom});
	my $chromStdev = &stdev($master{$chrom});
	my $chromLength = scalar @{$master{$chrom}};
	print STDOUT "chromosome = $chrom\nmean = $chromMean\nstdev = $chromStdev\n\n";


	my $combinedLength;
	for (my $n = 0; $n < $chromLength; $n++) {
		my $m = $n + 1;
		my $winDenominator = 0;
		if ($m%$win == 0) {		
			my $meanLength = $combinedLength / $win;
			my $rawZscore = ( $meanLength - $chromMean ) / $chromStdev ;
			my $zScore = sprintf ("%.3f", $rawZscore);
			my $start = $m - $win + 1;
			my $end = $m;
			print OUT "$chrom\t$start\t$end\t$zScore\n";
			if (defined @{$master{$chrom}}[$n]) {
				$combinedLength = @{$master{$chrom}}[$n];
				$winDenominator++;
			}
		}
		else {
			if (defined @{$master{$chrom}}[$n]) {
				$combinedLength += @{$master{$chrom}}[$n];
				$winDenominator++;
			}
		}
	}
}

close (OUT);


##### Subroutines #####

# adapted from https://edwards.sdsu.edu/research/calculating-the-average-and-standard-deviation/

sub average{
        my $data = shift @_;
        my $total = 0;
	my $count = 0;
	my $average;
        foreach my $value (@{$data}) {
		if (defined $value) {
	                $total += $value;
			$count++;
		}
        }
	if ($count > 0) {
	        $average = $total / $count;
	}
	else {
		$average = 0;
	}
        return $average;
}
sub stdev{
        my $data = shift @_;
        my $average = &average($data);
        my $sqtotal = 0;
	my $count = 0;
        foreach my $value (@{$data}) {
		if (defined $value) {
                	$sqtotal += ($average-$value) ** 2;
			$count++;
		}
        }
	if ($count < 2) {
		return 0;
	}
        else {
		my $std = ($sqtotal / ($count - 1)) ** 0.5;
        	return $std;
	}
}

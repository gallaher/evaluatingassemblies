#!/usr/bin/perl

use strict;
use warnings;

# This is a script to generate a data file
# of the percentage of reads that fall
# within various bins relative to the mean
# for windows of size $win
# from a sam file of paired-end reads.

# author: Sean D. Gallaher
# date:  21-APR-2017
# filename: sam2length.pl
# version: v1.3

# average accross windows of this many NTs
my $win = 1000;

# Output this help in case of error 
my $help = "\nPlease supply a sorted sam file of\npaired-end DNA-Seq data as an arguement.\n\n";

# Get the name of a sam file, or else exit with help.
my $sam = shift @ARGV;
unless (defined $sam) {
	die $help;
}

# Create a name for the output file.
$sam =~ m/(.+).sam/;
my $root = $1;
my $outfile = $root . ".fragment_length_bins_in_" . $win . "_NT_windows.tsv";


unless (defined $root) {
	die $help;
}

# Open the sam file and determine the median and standard deviation
# for the insert sizes for each position and put it into %master

open (IN, "<", $sam)
	or die "Cannot open $sam: $!\n";

my %master;
my @curPos;
my $lastPos = 0;
my $chrom;
my $curChrom;
my @chromOrder;
my %chromSizes;
my $length;
my $curLength;
my @allInsertSizes;
my $meanInsertSize;

my $maxInsert = 5000;

while (my $line = <IN>) {
	chomp $line;
	if ($line !~ m/^@/) {
		my @lineArray = split (/\t/, $line);
		$chrom = $lineArray[2];
		if ($chrom eq "*") {
			next;
		}
		if (!defined $curChrom) {
			$curChrom = $chrom;
			push (@chromOrder, $chrom);
		}
		my $position = $lineArray[3];
		$length = abs $lineArray[8];
		if (($length > 0) & ($length < $maxInsert)) {
			if ($chrom ne $curChrom) {
				$chromSizes{$curChrom} = $lastPos;
				$lastPos = 0;
				push (@chromOrder, $chrom);
				$curChrom = $chrom;
			}
			push (@{$master{$chrom}{$position}}, $length);
			push (@allInsertSizes, $length);
		}
		$lastPos = $position;		
	}
}
$chromSizes{$curChrom} = $lastPos;


close (IN);

# find the overall mean and print to STDOUT

$meanInsertSize=&average (\@allInsertSizes);

print STDOUT "The mean insert size is $meanInsertSize\n";



# establish the sizes of the bins

my $bin1lim = 0;
my $bin2lim = $meanInsertSize * 0.25;
my $bin3lim = $meanInsertSize * 0.5;
my $bin4lim = $meanInsertSize * 0.75;
my $bin5lim = $meanInsertSize * 1.25;
my $bin6lim = $meanInsertSize * 1.5;
my $bin7lim = $meanInsertSize * 1.75;
my $bin8lim = $meanInsertSize * 2;


# create the output file

open (OUT, ">", $outfile) 
	or die "Cannot open $outfile: $!\n\n";

print OUT "#chromosome\tstart\tend\t0\t0-0.25mean\t0.25-0.5mean\t0.5-0.75mean\t0.75-1.25mean\t1.25-1.5mean\t1.5-1.75mean\t1.75-2mean\t>2mean\n";

# for each window of size $win, determine the percentage of reads that fall in each bin

foreach my $chrom (@chromOrder) {
	my $chromLength = $chromSizes{$chrom};
	my $winCount = 0;
	my $bin1count = 0;
	my $bin2count = 0;
	my $bin3count = 0;
	my $bin4count = 0;
	my $bin5count = 0;
	my $bin6count = 0;
	my $bin7count = 0;
	my $bin8count = 0;
	my $bin9count = 0;
	for (my $n = 1; $n <= $chromLength; $n++) {
		if (($n%$win == 0) & ($n > 0)) {		
                        my $bin1percent = $bin1count / $winCount ;
                        my $bin2percent = $bin2count / $winCount ;
                        my $bin3percent = $bin3count / $winCount ;
                        my $bin4percent = $bin4count / $winCount ;
                        my $bin5percent = $bin5count / $winCount ;
                        my $bin6percent = $bin6count / $winCount ;
                        my $bin7percent = $bin7count / $winCount ;
                        my $bin8percent = $bin8count / $winCount ;
                        my $bin9percent = $bin9count / $winCount ;	
			my $start = $n - $win + 1;
			my $end = $n;
			my $bin1 = sprintf("%.3f", $bin1percent);
			my $bin2 = sprintf("%.3f", $bin2percent);	
			my $bin3 = sprintf("%.3f", $bin3percent);	
			my $bin4 = sprintf("%.3f", $bin4percent); 	
			my $bin5 = sprintf("%.3f", $bin5percent); 
			my $bin6 = sprintf("%.3f", $bin6percent); 
			my $bin7 = sprintf("%.3f", $bin7percent);   
			my $bin8 = sprintf("%.3f", $bin8percent);    
			my $bin9 = sprintf("%.3f", $bin9percent);    
			print OUT "$chrom\t$start\t$end\t$bin1\t$bin2\t$bin3\t$bin4\t$bin5\t$bin6\t$bin7\t$bin8\t$bin9\n";
			$winCount = 0;
			$bin1count = 0 ;
			$bin2count = 0 ;
			$bin3count = 0 ;
			$bin4count = 0 ;
			$bin5count = 0 ;
			$bin6count = 0 ;
			$bin7count = 0 ;
			$bin8count = 0 ;
			$bin9count = 0 ;
			if (defined @{$master{$chrom}{$n}}) {
				foreach my $size (@{$master{$chrom}{$n}}) {
					if ($size == $bin1lim) {
						$bin1count++;
						$winCount++;
					}
					elsif ($size < $bin2lim) {
						$bin2count++;
						$winCount++;
					}
					elsif ($size < $bin3lim) {
						$bin3count++;
						$winCount++;
					}
					elsif ($size < $bin4lim) {
						$bin4count++;
						$winCount++;
					}
					elsif ($size < $bin5lim) {
						$bin5count++;
						$winCount++;
					}
					elsif ($size < $bin6lim) {
						$bin6count++;
						$winCount++;
					}
					elsif ($size < $bin7lim) {
						$bin7count++;
						$winCount++;
					}
					elsif ($size < $bin8lim) {
						$bin8count++;
						$winCount++;
					}
					elsif ($size >= $bin8lim) {
						$bin9count++;
						$winCount++;
					}
					else {
						print STDOUT "There is a problem with the insert sizes a $chrom : $n \n";
					}	
				}
			}
		}
		else {
			if (defined @{$master{$chrom}{$n}}) {
				foreach my $size (@{$master{$chrom}{$n}}) {
					if ($size == $bin1lim) {
						$bin1count++;
						$winCount++;
					}
					elsif ($size < $bin2lim) {
						$bin2count++;
						$winCount++;
					}
					elsif ($size < $bin3lim) {
						$bin3count++;
						$winCount++;
					}
					elsif ($size < $bin4lim) {
						$bin4count++;
						$winCount++;
					}
					elsif ($size < $bin5lim) {
						$bin5count++;
						$winCount++;
					}
					elsif ($size < $bin6lim) {
						$bin6count++;
						$winCount++;
					}
					elsif ($size < $bin7lim) {
						$bin7count++;
						$winCount++;
					}
					elsif ($size < $bin8lim) {
						$bin8count++;
						$winCount++;
					}
					elsif ($size >= $bin8lim) {
						$bin9count++;
						$winCount++;
					}
					else {
						print STDOUT "There is a problem with the insert sizes a $chrom : $n \n";
					}
				}
			}
		}	
	}
}

	
close (OUT);


##### Subroutines #####

# adapted from https://edwards.sdsu.edu/research/calculating-the-average-and-standard-deviation/

sub average{
        my $data = shift @_;
        my $total = 0;
	my $count = 0;
	my $average;
        foreach my $value (@{$data}) {
		if (defined $value) {
	                $total += $value;
			$count++;
		}
        }
	if ($count > 0) {
	        $average = $total / $count;
	}
	else {
		$average = 0;
	}
        return $average;
}
sub stdev{
        my $data = shift @_;
        my $average = &average($data);
        my $sqtotal = 0;
	my $count = 0;
        foreach my $value (@{$data}) {
		if (defined $value) {
                	$sqtotal += ($average-$value) ** 2;
			$count++;
		}
        }
	if ($count < 2) {
		return 0;
	}
        else {
		my $std = ($sqtotal / ($count - 1)) ** 0.5;
        	return $std;
	}
}

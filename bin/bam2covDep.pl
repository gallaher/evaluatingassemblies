#!/usr/bin/perl

use strict;
use warnings;

# This is a script to generate a table
# of the percentage of loci whose depth 
# of coverage fall within various bins relative 
# to the mean coverage over 1 kbp windows
# from a sam file of 
# DNA-Seq reads.
# author: Sean D. Gallaher
# date:  04-APR-2017
# filename: bam2covDep.pl
# version: v1.0

# determine bins accross windows of this many NTs
my $win = 1000;

# Output this help in case of error 
my $help = "\nPlease supply a sorted bam file of\nDNA-Seq data as an arguement.\n\nScript expects bedtools is in your \$PATH\n\n";

# Get the name of a bam file, or else exit with help.
my $bam = shift @ARGV;
unless (defined $bam) {
	die $help;
}

# Create a name for the output file.
$bam =~ m/(.+).bam/;
my $root = $1;
my $baseReport = $root . ".per-base_report.txt";
my $outfile = $root . ".coverage_bins_" . $win . "_bp_windows.tsv";


unless (defined $root) {
	die $help;
}

# If necessary, use bedtools to make a per-base repport of coverage depth.

if (!-e $baseReport) {
	my $bashCommand = qq {bedtools genomecov -d -split -ibam $bam > $baseReport };  
	system $bashCommand ;
}

# Open the baseReport, and transfer the
# the coverage per position to %master

my %master;

open (REPORT, "<", $baseReport) 
	or die "Cannot open $baseReport: $!\n\n$help";

while (my $line = <REPORT>) {
	chomp $line;
	my @lineArray = split (/\t/, $line);
	my $chrom = $lineArray[0];
	my $pos = $lineArray[1];
	my $cov = $lineArray[2];
	@{$master{$chrom}}[$pos]=$cov;
}

close (REPORT);

# create the output file

open (OUT, ">", $outfile) 
	or die "Cannot open $outfile: $!\n\n";


print OUT "#chromosome\tstart\tend\t0\t0-0.25mean\t0.25-0.5mean\t0.5-0.75mean\t0.75-1.25mean\t1.25-1.5mean\t1.5-1.75mean\t1.75-2mean\t>2mean\n";

# for each window of size $win

foreach my $chrom (sort keys %master) {
	my $null = shift @{$master{$chrom}};
	my $chromMean = &average($master{$chrom});
	my $chromStdev = &stdev($master{$chrom});
	my $chromLength = scalar @{$master{$chrom}};
	print STDOUT "chromosome = $chrom\nmean = $chromMean\nstdev = $chromStdev\n\n";
	my $winCount = 0;
	my $bin1lim = 0;
	my $bin2lim = $chromMean * 0.25;
	my $bin3lim = $chromMean * 0.5;
	my $bin4lim = $chromMean * 0.75;
	my $bin5lim = $chromMean * 1.25;
	my $bin6lim = $chromMean * 1.5;
	my $bin7lim = $chromMean * 1.75;
	my $bin8lim = $chromMean * 2;
	my $bin1count = 0;
	my $bin2count = 0;
	my $bin3count = 0;
	my $bin4count = 0;
	my $bin5count = 0;
	my $bin6count = 0;
	my $bin7count = 0;
	my $bin8count = 0;
	my $bin9count = 0;
	for (my $n = 0; $n < $chromLength; $n++) {
		my $m = $n + 1;
		if (($n%$win == 0 ) & ($n > 0)) {		
			my $bin1percent = $bin1count / $winCount ; 
			my $bin2percent = $bin2count / $winCount ; 
			my $bin3percent = $bin3count / $winCount ; 
			my $bin4percent = $bin4count / $winCount ; 
			my $bin5percent = $bin5count / $winCount ; 
			my $bin6percent = $bin6count / $winCount ; 
			my $bin7percent = $bin7count / $winCount ; 
			my $bin8percent = $bin8count / $winCount ; 
			my $bin9percent = $bin9count / $winCount ; 
			my $start = $m - $win;
			my $end = $n;
			print OUT "$chrom\t$start\t$end\t$bin1percent\t$bin2percent\t$bin3percent\t$bin4percent\t$bin5percent\t$bin6percent\t$bin7percent\t$bin8percent\t$bin9percent\n";
			if (defined @{$master{$chrom}}[$n]) {
				my $locusDepth = @{$master{$chrom}}[$n];
				$winCount = 1;
				$bin1count = 0;
				$bin2count = 0;
				$bin3count = 0;
				$bin4count = 0;
				$bin5count = 0;
				$bin6count = 0;
				$bin7count = 0;
				$bin8count = 0;
				$bin9count = 0;
				if ($locusDepth == $bin1lim) {
					$bin1count++;
				}
				elsif ($locusDepth < $bin2lim) {
					$bin2count++;
				}
				elsif ($locusDepth < $bin3lim) {
					$bin3count++;
				}
				elsif ($locusDepth < $bin4lim) {
					$bin4count++;
				}
				elsif ($locusDepth < $bin5lim) {
					$bin5count++;
				}
				elsif ($locusDepth < $bin6lim) {
					$bin6count++;
				}
				elsif ($locusDepth < $bin7lim) {
					$bin7count++;
				}
				elsif ($locusDepth < $bin8lim) {
					$bin8count++;
				}
				elsif ($locusDepth >= $bin8lim) {
					$bin9count++;
				}
				else {
					print STDOUT "There is a problem with coverage depth $locusDepth\n";
				}
			}
		}
		else {
			if (defined @{$master{$chrom}}[$n]) {
				my $locusDepth = @{$master{$chrom}}[$n];
				if ($locusDepth == $bin1lim) {
					$bin1count++;
				}
				elsif ($locusDepth < $bin2lim) {
					$bin2count++;
				}
				elsif ($locusDepth < $bin3lim) {
					$bin3count++;
				}
				elsif ($locusDepth < $bin4lim) {
					$bin4count++;
				}
				elsif ($locusDepth < $bin5lim) {
					$bin5count++;
				}
				elsif ($locusDepth < $bin6lim) {
					$bin6count++;
				}
				elsif ($locusDepth < $bin7lim) {
					$bin7count++;
				}
				elsif ($locusDepth < $bin8lim) {
					$bin8count++;
				}
				elsif ($locusDepth >= $bin8lim) {
					$bin9count++;
				}
				else {
					print STDOUT "There is a problem with coverage depth $locusDepth\n";
				}
				$winCount++;
			}
		}
	}
}

close (OUT);


##### Subroutines #####

# adapted from https://edwards.sdsu.edu/research/calculating-the-average-and-standard-deviation/

sub average{
        my $data = shift @_;
        my $total = 0;
	my $count = 0;
	my $average;
        foreach my $value (@{$data}) {
		if (defined $value) {
	                $total += $value;
			$count++;
		}
        }
	if ($count > 0) {
	        $average = $total / $count;
	}
	else {
		$average = 0;
	}
        return $average;
}
sub stdev{
        my $data = shift @_;
        my $average = &average($data);
        my $sqtotal = 0;
	my $count = 0;
        foreach my $value (@{$data}) {
		if (defined $value) {
                	$sqtotal += ($average-$value) ** 2;
			$count++;
		}
        }
	if ($count < 2) {
		return 0;
	}
        else {
		my $std = ($sqtotal / ($count - 1)) ** 0.5;
        	return $std;
	}
}

#!/usr/bin/perl

use warnings;
use strict;

# This is a script to go through a SAM file
# and the corresponding FASTA file and 
# calculate the error frequency 
# in a specified window
# and output that as a bedgraph file.
# Author: 	Sean Gallaher
# Date:   	26-APR-2017
# File:    	sam2errorFreq.pl
# Version: 	v1.3


# Window Size:
my $win = 1000;

# error message
my $help = "\nPlease specify a fasta file and\nsorted sam file as arguements.\n\n";

my $fasta = shift @ARGV;
my $sam = shift @ARGV;


if (!defined $sam) {
	die $help;
}

open (FA, "<", $fasta) 
	or die "Cannot open $fasta: $!\n";

my %master;
my $chrom;
my $seq;

while (my $line = <FA>) {
	chomp $line;
	if ($line =~ m/^>(.+)/) {
		if (defined $chrom) {
			my @seqArray = split (//, $seq);
			unshift (@seqArray, "-");
			@{$master{$chrom}} = @seqArray;
			$chrom = $1;
			$seq = undef;
		}
		else {
			$chrom = $1;
		}
	}
	else {
		$seq .= $line;
	}
}

my @seqArray = split (//,$seq);
unshift (@seqArray, "-");
@{$master{$chrom}} = @seqArray;


close (FA);

### go through the sam file. For each read sequence,
### add 1 to the total hash for each NT.
### add 1 to the error hash if the NT does not match the
### reference sequence or if it is an InDel 
### based on the CIGAR
### Soft- and Hard-clipped NTs are ignored.

open (SAM, "<", $sam)
	or die "Cannot open $sam: $!\n"; 

my %errors;
my %total;
my @chromOrder;

while (my $line = <SAM>) {
	chomp $line;
	if ($line !~ m/^@/) {
		my @lineArray = split (/\t/, $line);
		my $chrom = $lineArray[2];
		my $refPos = $lineArray[3];
		my $read = $lineArray[9];
		my $cigar = $lineArray[5];
		if ((defined $master{$chrom}) & ($cigar ne "*")) {
			my $chromCount = scalar @chromOrder;
			if ($chromCount == 0) {
				unshift (@chromOrder, $chrom);
			}		
			elsif ($chromOrder[0] ne $chrom) {
				unshift (@chromOrder, $chrom);
			}
			my @cigarArray = split (//, $cigar);
			my $cigLen = scalar @cigarArray;
			my @clipArray;
			my $count;
			for (my $j = 0; $j < $cigLen; $j++) {
				my $character = $cigarArray[$j];
				if ($character =~ m/[MIDNSH]/) {
					for (my $k = 0; $k < $count; $k++) {
						push (@clipArray, $character);
					}
					$count = undef;
				}
				if ($character =~ m/\d/) {
					$count .= $character;
				}
	
			}
			my @readArray = split (//, $read);
			my $len = scalar @readArray;
			for (my $i = 0; $i < $len ; $i++) {
				my $cigarCat = shift (@clipArray);
				if ($cigarCat =~ m/M/) {
					my $readNT = shift (@readArray);
					my $refNT = ${$master{$chrom}}[$refPos];
					if ((defined $readNT) & (defined $refNT)) {
						if ($readNT eq $refNT) {
							${$total{$chrom}}[$refPos]++;
						}
						else {
							${$total{$chrom}}[$refPos]++;
							${$errors{$chrom}}[$refPos]++;
						}
					}
					$refPos++;
				}
				elsif ($cigarCat =~ m/[HS]/) {
					my $null = shift (@readArray);
				}
				elsif ($cigarCat =~ m/I/) {
					${$errors{$chrom}}[$refPos]++;
					my $null = shift (@readArray);
				}
				elsif ($cigarCat =~ m/D/) {
					${$errors{$chrom}}[$refPos]++;
					$refPos++;
				}
			}
		}
	}
}



close (SAM);

### calculate the error rate in $win sized windows 
### and print the result to a bedgraph file

$sam =~ m/(.+).sam/;
my $rootName = $1;
my $outBed = $rootName . ".error_rate_" . $win . "_NT_windows.bedgraph";

open (BED, ">", $outBed)
	or die "Cannot open $outBed: $!\n";

print BED "track type=bedGraph\n";

my $chromCount = scalar (@chromOrder);

for (my $l = 0; $l < $chromCount; $l++) {
	my $chrom = pop (@chromOrder);
	my $chromCoverage;
	my $chromErrors;
	my $winCoverage;
	my $winErrors;
	my $lociCount = scalar @{$total{$chrom}};
	my $lastWinEnd;
	for (my $m = 0; $m < $lociCount; $m++) {
		my $pos = $m + 1;
		if (defined ${$total{$chrom}}[$pos]) {
			$chromCoverage += ${$total{$chrom}}[$pos];
			$winCoverage += ${$total{$chrom}}[$pos];
		}
		if (defined ${$errors{$chrom}}[$pos]) {
			$chromErrors += ${$errors{$chrom}}[$pos];
			$winErrors += ${$errors{$chrom}}[$pos];
		}
		if ( (($m > 0) & ( ($pos%$win == 0)|($pos == $lociCount) )) | ($win == 1)) {
			my $winErrorRate;
			if ($winCoverage == 0) {
				$winErrorRate = "no coverage";
			}
			else {		
				my $winErrorRateRaw = $winErrors / $winCoverage ;
				if ($winErrorRateRaw > 1) {
					$winErrorRateRaw = 1;
				}
				$winErrorRate = sprintf("%.5f", $winErrorRateRaw);
			}
			my $winStart;
			if ($pos == $lociCount) {
				$winStart = $lastWinEnd + 1;	
			}
			else {
				$winStart = $pos - $win + 1;
			}
			if ($winStart < 1) {
				$winStart = 1;
			}
			print BED "$chrom\t$winStart\t$pos\t$winErrorRate\n";
			$winCoverage = 0;
			$winErrors = 0;
			$lastWinEnd = $pos;
		}
	}	
	my $chromErrorRateRaw = $chromErrors / $chromCoverage ;
	my $chromErrorRate = sprintf("%.5f", $chromErrorRateRaw);
	print STDOUT "chromosome = $chrom\ncovered bases = $lociCount\ntotal coverage = $chromCoverage\nerrors = $chromErrors\nerror rate = $chromErrorRate\n\n";
}


close (BED);





